package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        String toReturn = "";
        for (String s : args){
            toReturn += s;
            toReturn += " ";
        }

        return toReturn;
    }

    @Override
    public String getName() {
        
        return "echo";
    }
    
}
